package ZalandoPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends AbstractPage {

    private static By BANNER_OK_BUTTON = By.xpath("//button[@id='uc-btn-accept-banner']");

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void clickOnBanner(){
        click(BANNER_OK_BUTTON);
    }
}
